﻿namespace RealtimeGrass.Rendering
{
    /// <summary>
    /// Describes the primitive type represented in vertex data.
    /// </summary>
    public enum PrimitiveTopology
    {
        /// <summary>
        /// The vertex data represents a point list.
        /// </summary>
        PointList,

        /// <summary>
        /// The vertex data represents a line list.
        /// </summary>
        LineList,

        /// <summary>
        /// The vertex data represents a triangle list.
        /// </summary>
        TriangleList
    }
}